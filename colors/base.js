module.exports = {
  ucBlue: {
    100: '#e9f2f9',
    200: '#a4cbe1',
    300: '#4fabf6',
    400: 'transparent',
    500: '#015999',
    600: '#1540a2',
    700: '#03357a',
    800: 'transparent',
    900: '#000e2f'
  },
  ucCoolGrey: {
    100: '#f0f3f7',
    200: '#a2aaad',
    300: '#7c878e',
    400: 'transparent',
    500: '#6b7880',
    600: 'transparent',
    700: 'transparent',
    800: 'transparent',
    900: 'transparent',
  },
  ucGrey: {
    100: '#eeeeee',
    200: '#ebebeb',
    300: '#cccccc',
    400: '#999999',
    500: '#808080',
    600: '#777777',
    700: '#555555',
    800: '#333333',
    900: '#1a1a1a',
  },
  ucRed: {
    100: 'transparent',
    200: '#f05b4f',
    300: '#e4002b',
    400: '#c42b29',
    500: 'transparent',
    600: '#b10200',
    700: '#8a2121',
    900: '#6e0015',
    900: 'transparent',
  }
}




