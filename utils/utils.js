/**
 * 
 * Adds custom UConn properties to the default tailwind set while removing duplicates
 * 
 * @param {array} uconn an array of custom UConn properties
 * @param {array} tw an array of tailwind config properties
 * @returns {array} the merged values
 */
const mergeVariables = (uconn, tw) => {
  return [
    ...uconn,
    ...tw
  ].reduce((acc, cur) => {
    if (!acc.includes(cur)) {
      acc.push(cur)
    }
    return acc
  }, [])
}

module.exports = {
  mergeVariables
}