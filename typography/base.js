module.exports = {
  uconnSans: [
    '"Proxima Nova"',
    'Verdana',
    'Arial',
    'Helvetica',
    'sans-serif'
  ],
  uconnSerif: [
    'Georgia',
    'serif'
  ],
}