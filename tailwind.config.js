const { ucBlue, ucCoolGrey, ucGrey, ucRed } = require('./colors/base')

module.exports = {
  content: [
    './index.html'
  ],
  theme: {
    extend: {
      colors: {
        ucBlue: {
          100: ucBlue['100'],
          200: ucBlue['200'],
          300: ucBlue['300'],
          400: ucBlue['400'],
          500: ucBlue['500'],
          600: ucBlue['600'],
          700: ucBlue['700'],
          800: ucBlue['800'],
          900: ucBlue['900'],
        },
        ucCoolGrey: {
          100: ucCoolGrey['100'],
          200: ucCoolGrey['200'],
          300: ucCoolGrey['300'],
          400: ucCoolGrey['400'],
          500: ucCoolGrey['500'],
          600: ucCoolGrey['600'],
          700: ucCoolGrey['700'],
          800: ucCoolGrey['800'],
          900: ucCoolGrey['900'],
        },
        ucGrey: {
          100: ucGrey['100'],
          200: ucGrey['200'],
          300: ucGrey['300'],
          400: ucGrey['400'],
          500: ucGrey['500'],
          600: ucGrey['600'],
          700: ucGrey['700'],
          800: ucGrey['800'],
          900: ucGrey['900'],
        },
        ucRed: {
          100: ucRed['100'],
          200: ucRed['200'],
          300: ucRed['300'],
          400: ucRed['400'],
          500: ucRed['500'],
          600: ucRed['600'],
          700: ucRed['700'],
          800: ucRed['800'],
          900: ucRed['900'],
        }        
      }
    },
  },
  plugins: [],
}
